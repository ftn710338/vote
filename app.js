
var fs = require('fs'); 

var express = require('express');
const { createProxyMiddleware  } = require('http-proxy-middleware')  //这个依赖的版本为1.0以上写法
// const proxy = require('http-proxy-middleware')  //1.0一下这样写也可以，如果1.0以上的话这种写法会报错“proxy不是一个方法”


var app = express();
 
app.use('/', express.static('public'));

app.use('/beanFunApi', createProxyMiddleware({ 
  // 转发到5000端口
  target: 'https://atomboyz.beanfun.com/api/people',  //你访问的接口地址
  // 转发时重写路径
  pathRewrite: {'^/beanFunApi' : ''},
  changeOrigin: true }))
 
var server = app.listen(14000, function () {
 
  var host = server.address().address
  var port = server.address().port
})



